# AI-on-Microsoft-Azure

| Zagadnienie                                                  | Część teoretyczna                                            | Część praktyczna                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------ |
| Evaluate text with Azure Cognitive Language Services         | [link](Azure-Cognitive-Language-Services.md)                 | x                                    |
| Process and Translate Speech with Azure Cognitive Speech Services | [link](Process and Translate Speech with Azure-Cognitive-Speech-Services.md) | x                                    |
| Azure Bot Service                                            | [link](Azure-Bot-Service.md)                                 | [link](Bot/README.md)                |
| Process and classify images with the Azure cognitive vision services | [link](Process-and-classify-images-with-the-Azure-cognitive-vision-services.md) | [link](DogBreedRecognizer/README.md) |
| Automated (Auto) ML                                          | x                                                            | [link](AML/learn-aml.ipynb)          |

