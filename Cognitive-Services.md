## Część teoretyczna

#### TLTR

`Azure Cognitive Services` jest to grupa usług oferowana przez Azure. Serwis ten umożliwia realizowanie wybranych zmysłów poznawczych człowieka w systemach informatycznych przez osobę nie zagłębioną w te zagadnienia. 

#### Wstęp

Przykładowymi zmysłami poznawczymi jest: podejmowanie decyzji, wyszukiwanie informacji, rozpoznawanie mowy, analiza obrazów. Realizację tych zmysłów zajmują się zagadnienia *sztucznej inteligencji (ang. artificial intelligence)*. Najczęściej wykorzystuje się jeden z jej działów - *uczenie maszynowe (ang. machine learning)*. 

Azure dostarcza kilka serwisów, w których można zrealizować uczenie maszynowe: [Azure Machine Learning](https://docs.microsoft.com/pl-pl/azure/architecture/data-guide/technology-choices/data-science-and-machine-learning?context=azure/machine-learning/studio/context/ml-context#azure-machine-learning), [Azure Cognitive Services](https://docs.microsoft.com/pl-pl/azure/architecture/data-guide/technology-choices/data-science-and-machine-learning?context=azure/machine-learning/studio/context/ml-context#azure-cognitive-services), [Azure SQL Managed Instance Machine Learning Services](https://docs.microsoft.com/pl-pl/azure/architecture/data-guide/technology-choices/data-science-and-machine-learning?context=azure/machine-learning/studio/context/ml-context#sql-machine-learning), [Machine learning in Azure Synapse Analytics](https://docs.microsoft.com/pl-pl/azure/architecture/data-guide/technology-choices/data-science-and-machine-learning?context=azure/machine-learning/studio/context/ml-context#sql-machine-learning), [Azure Databricks](https://docs.microsoft.com/pl-pl/azure/architecture/data-guide/technology-choices/data-science-and-machine-learning?context=azure/machine-learning/studio/context/ml-context#azure-databricks). 

Serwisem, który umożliwia realizacje zmysłów poznawczych bez znajomość zagadnień machine learning oraz data science jest [`Azure Cognitive Services`](https://azure.microsoft.com/pl-pl/services/cognitive-services/#overview). Oferuje on wstępnie zaimplementowane rozwiązania sztucznej inteligencji, które są udostępnione za pośrednictwem REST API oraz SDKs. Pozwala to na szybkie zbudowanie inteligentnej aplikacji wykorzystując jest popularne języki programowania takie jak C#, Java, JavaScript, Python.

#### Usługi zawarte w `Azure Cognitive Services`

| Kategoria usług | Usługa                                                       | Opis                                                         | Koszt (dla West Europe)                                      |
| --------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Vision          | [Computer Vision](https://azure.microsoft.com/en-us/services/cognitive-services/computer-vision/) | Zawiera gotowe algorytmy analizujące elementy zawarte w obrazach oraz filmach. Pozwala na np. wydobywanie tekstu z obrazów, rozpoznawanie elementów na obrazach, analizowanie poruszanie się ludzi w czasie rzeczywistym. | 5k transakcji miesięcznie za darmo. Od $0.65 do $2.5 za każde 1k transakcja (w zależności od opcji oraz liczby wszystkich transakcja) |
|                 | [Custom Vision](https://azure.microsoft.com/en-us/services/cognitive-services/custom-vision-service/) | Pozwala na utworzenie własnego algorytmu analizującego obraz z wykorzystaniem bazy obrazów uczących. | Z pewnymi ograniczeniami wersja bezpłatna. Powyżej limitów opłaty za ilość transakcji, godzin użytych na uczenie, zajmowanie pamięci przez zdjęcia wykorzystywane do uczenia. |
|                 | [Face](https://azure.microsoft.com/en-us/services/cognitive-services/face/) | Umożliwia rozpoznawanie twarzy oraz wyodrębnienie różnych informacji, takich jak między innymi położenie na zdjęciu, wiek, emocje. Możliwa jest również identyfikacja osób. | 30k transakcji miesięcznie za darmo. Od $0.40 do $1 za każde 1k transakcja (w zależności od opcji oraz liczby wszystkich transakcja) |
|                 | [Form Recognizer](https://azure.microsoft.com/en-us/services/cognitive-services/form-recognizer/) | Wydobywanie tekstu oraz formy z dokumentów w formie elektronicznej bądź jako zdjęcie wydrukowanego tekstu. | 500 stron za darmo. $50 lub $10 za każde 1k stron (w zależności od typu dokumentu - własny lub z szablonu) |
|                 | [Video Indexer](https://azure.microsoft.com/en-us/services/media-services/video-indexer/) | Zestaw funkcji rozszerzający prowadzenie biblioteki filmów. Pozwala on na między innymi generowanie napisów w różnych językach czy rekomendowanie innych filmów na podstawie obiektów i osób zawartych w poprzednich filmach. | 40h za darmo. Powyżej limitów opłaty w zależności od wybranych opcji. |
| Speech          | [Speech to Text](https://azure.microsoft.com/en-us/services/cognitive-services/speech-to-text/) | Konwersja mowy na tekst.                                     | 5h miesięcznie za darmo. Od $1 do $2.1 za każdą godzinę (w zależności od opcji). |
|                 | [Text to Speech](https://azure.microsoft.com/en-us/services/cognitive-services/text-to-speech/) | Konwersja tekstu na mowę. W bazie znajdują się zarówno głosy lektorów oraz wygenerowane przez sieci. | 0.5M lub 5M znaków miesięcznie za darmo. Od $4 do $100 za każdy 1M znaków (w zależności od opcji). |
|                 | [Speech Translation](https://azure.microsoft.com/en-us/services/cognitive-services/speech-translation/) | Tłumaczenie mowy.                                            | 5h miesięcznie za darmo. $2.5 za każdą godzinę.              |
|                 | [Speaker Recognition](https://azure.microsoft.com/en-us/services/cognitive-services/speaker-recognition/) | Identyfikacja osoby mówiącej.                                | 10k transakcji miesięcznie za darmo. $5 lub $10 za każde 1k transakcja (w zależności od opcji). Opcja dostępna tylko w West US |
| Language        | [Immersive Reader](https://azure.microsoft.com/en-us/services/cognitive-services/immersive-reader/) | Narzędzie pomagające osobom z trudnościami przeczytać oraz zrozumieć tekst. Przykładowo wyrazy, które algorytm uznał za istotne mają inny kolor. | 3M znaków miesięcznie za darmo. $10 lub $5 za każdy 1M znaków w zależności czy zastosowanie standardowe czy edukacyjne/nonprofit. |
|                 | [Language Understanding](https://azure.microsoft.com/en-us/services/cognitive-services/language-understanding-intelligent-service/) | Umożliwia ona interpretację ludzkiego języka oraz wyciąganie kluczowych informacji. | 10k transakcji analizy tekstu oraz 1M autoryzacji transakcji miesięcznie za darmo. $1.5 za każde 1k transakcji analizy tekstu. $5.5 za każde 1k transakcji analizy mowy. |
|                 | [QnA Maker](https://azure.microsoft.com/en-us/services/cognitive-services/qna-maker/) | Usługa umożliwia odpowiadanie na pytania, których odpowiedzi zostały już udzielone. Nieustanie się uczy na podstawie zachowania użytkowników. | Usługa wykorzystuje kilka usług.                             |
|                 | [Text Analytics](https://azure.microsoft.com/en-us/services/cognitive-services/text-analytics/) | Analiza tekstów nieposiadających struktury, formy. Przykładowo wydobywanie spostrzeżeń, obserwacji użytkowników na podstawie opinii danego produktu. | 5k transakcji miesięcznie za darmo. Od $0.25 do $2 za każde 1k transakcja (w zależności od ilości transakcji). Również możliwość uruchomienia na maszynach S0/S1/S2/S3/S4. |
|                 | [Translator](https://azure.microsoft.com/en-us/services/cognitive-services/translator/) | Tłumacz tekstu czasu rzeczywistego.                          | 2M transakcji miesięcznie za darmo. Pozostałe rozliczanie zbyt różnorodne aby zawrzeć tutaj. |
| Decision        | [Anomaly Detector](https://azure.microsoft.com/en-us/services/cognitive-services/anomaly-detector/) | Detekcja anomalii na podstawie danych zawierających zmianę wartości poszczególnych parametrów w czasie. Anomalia jest definiowana na podstawie aktualnej wartości oraz wartości wyznaczonej na podstawie trendu. | 20k transakcji miesięcznie za darmo. $0.314 za każde 1k transakcji. |
|                 | [Content Moderator](https://azure.microsoft.com/en-us/services/cognitive-services/content-moderator/) | Wspomaganie moderowania treści. Detekcja potencjalnie obraźliwych, niepożądanych obrazów, tekstów oraz filmów wideo. | Po 5k transakcji miesięcznie za darmo dla trybu "skromnego" oraz "recenzji". Od $0.4 do $1 za każde 1k transakcji (w zależności od całkowitej liczy transakcji). |
|                 | [Metrics Advisor](https://aka.ms/GualalaACOM)                | Usługa prewencyjnie monitorująca zdefiniowane wskaźniki. W przypadku zauważenia problemu, namierza kluczowy element mogący być źródłem problemu. | Aktualnie brak opłat - aktualnie usługa dostarczana jest w trybie pokazowym. |
|                 | [Personalizer](https://azure.microsoft.com/en-us/services/cognitive-services/personalizer/) | Pomaga personalizować produkt na podstawie informacji posiadanych na temat danego użytkownika | 50k transakcji miesięcznie za darmo (limit 10GB). Od $0.05 do $1 za każdy 1k transakcji (w zależności od opcji, limit 10GB na każde 1M transakcji. |



### Use cases

* System monitoringu zastępujący częściowo pracownika ochrony - usługa Computer Vision
* Adaptacyjna automatyka domowa uwzględniająca preferencje każdego z domowników - Speech to Text, Text to Speech, Speaker Recognition, Language Understanding, Personalizer
* System wspomagający infolinię sanepidu - QnA Maker, Speech to Text, Text to Speech, Language Understanding

### How to

1. Posiadając konto Azure, tworzymy wybrany zasób serwisu na stronie https://portal.azure.com/
2. Odczytujemy zmienne środowiskowe: klucz oraz endpoint znajdujące się w utworzony serwisie.
3. Wykonujemy zapytania do uzyskanego endpoint'a, zamieszczając uprzednio w hederze nasz klucz.



