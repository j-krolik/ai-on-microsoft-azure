# Azure Bot Services

W dzisiejszym świecie ludzie często porozumiewają się za pośrednictwem różnych kanałów komunikacyjnych takich jak: rozmowa głosowa, email, media społecznościowe. Komunikacja ta może zachodzić w relacjach zarówno prywatnych jak i służbowych. Niemalże, każda aktywnie działająca organizacja w celu zapewnienia dobrego przepływu informacji z osobami z poza niej, musi zapewnić minimum jeden kanał komunikacji. Przykładowo sklepy internetowe na swoich stronach internetowych często umieszczają formularz zgłoszeniowy, adres e-mailowy, numer telefony. Niestety utrzymanie każdego kanału komunikacyjnego generuje koszty takie jak utrzymanie infrastruktury czy zasobów ludzkich.

Z tego powodu Microsoft Azure stworzył serwisy umożlwiające dodanie do aplikacji bota wspartego sztuczną inteligencją, który może zastępować człowieka na pierwszej linii wsparcia. 

* **QnA Maker** 
* **Azure Boot Servie**

## QnA Makrer

### Intro

Umożliwia utworzenie oraz opublikowanie bazy wiedzy Q&A (pytań i odpowiedzi) z wbudowanymi możliwościami przetwarzania języka naturalnego. Baza ta może być tworzona i trzymywana zarówno za pomocą dedykowanego *QnA Maker portal* (stronie internetowej), QnA Maker API oraz udostępnionego SDK. Ze względu na prostą obsługę, QnA Maker portal jest zalecany.

Dużą zaletą tego serwisu jest możliwość skorzystania z `kilku źródeł` tworzenia tej bazy:

* Importowanie FAQ z dokumentu lub strony internetowej
* Importowanie na podstawie istniejących konwersacji.
* Wprowadzanie oraz edytowanie manualne.

Co ważne zastosowanie mechanizmów przetwarzania języka naturalnego rozpoznaje cel pytania. Dzięki czemu można przewidzieć różne sposoby zadawania tego pytania poprzez dodanie alternatywnego sformułowania. Przykładowo posiadając pytanie "Jaka jest lokalizacja siedziby firmy?" możemy mieć formy alternatywne takie jak "Jakie jest położenie siedziby firmy?"

Utworzona baza wiedzy jest udostępniana za pomocą REST API. Aby aplikacja klienta mogła mieć dostęp do bazy wiedzy wymagany jest:

* posiadanie ID bazy wiedzy
* dostęp do końcówki (ang. endpoint) bazy wiedzy
* posiadanie klucza autoryzacyjnego danej bazy wiedzy

###  Use cases

* Pierwsza linia wsparcia
  * witryna pomagająca zdiagnozować awarię, np. brak internetu
  * wsparcie osób próbujących załatwić sprawę urzędową
* Sekcja strony internetowej dowolnej firmy np. start-up (Czym firma się zajmuje?)

### How to

#### Jak użyć?

Aby skorzystać z QnA Maker'a należy posiadać ważną subskrypcję Azure.

Proces tworzenia można podzielić na fazy:

1. Generowanie bazy bazy wiedzy, [wnamaker.ai/create](https://www.qnamaker.ai/create)
   
   Na początku przeniesie nas na [protal.azure.com](portal.azure.com/#home), aby utworzyć odpowiedni zasób, należy dodać odpowiednie par pytanie&odpowiedź
2. Uczenie oraz testowanie
   Proces ten analizuje iteracyjnie każde pytanie po kolei z wykorzystaniem wbudowanych mechanizmów językowych. Tworzone są alternatywne formy pytań. W przypadku wykorzystywania portalu QnA Marker, można od razu przetestować utworzoną bazę wiedzy
3. Publikacja

#### Pricing (West Europe)

Opłaty związane związane z wykorzystywaniem serwisu QnAM Marker, zarówno API jak i portalu.

| Instancja | Liczba transakcji na sekundę (TPS) | Ograniczenia                                                 | Koszt                                                       |
| --------- | ---------------------------------- | ------------------------------------------------------------ | ----------------------------------------------------------- |
| Bezpłatna | 3TPS                               | Do 1MB każdy dokument <br />Do 100 transakcji na minutę<br />Do 50k transakcji miesięcznie | Bezpłatne zarządzanie maksymalnie 3 dokumentami miesięcznie |
| Standard  | 3TPS                               | Do 100 transakcji na minutę                                  | $10 - Nielimitowana ilość zarządzanych dokumentów           |

Dodatkowe opłaty związane z wykorzystywaniem 

* Azure App Service - wykorzystywanie mocy obliczeniowej, szczegółowy [cennik](https://azure.microsoft.com/en-us/pricing/details/app-service/windows/)
* Azure Cognitive Search - przechowywanie danych, szczegółowy [cennik](https://azure.microsoft.com/en-us/pricing/details/search/)

## Azure Boot Service

### Intro

**Azure Boot Servie** jest to serwis dostarczający framework umożliwiający tworzenie, rozwój oraz zarządzanie botami na Azure. Może on stanowić między innymi rozszerzać funkcjonalność jaką dostarcza QnA Marker. 

###  Use cases

* Interaktywna pierwsza linia wsparcia
  * czat z botem pomagający zdiagnozować awarię, np. brak internetu
  * infolinia w sanepidzie, z możliwością sprawdzenia wyniku testu oraz uzyskania podstawowych informacji
  * wsparcie osób próbujących załatwić sprawę urzędową

### How to

#### Jak użyć?

Aby skorzystać z QnA Maker'a należy posiadać ważną subskrypcję Azure.

Do obsługi bota dostarczone jest Microsoft Bot Framework SDK. W przypadku łączenia go z QnA Marker'em można wykorzystać dołączone do niego środowisko.

Generalnie nie sposób opisać tego procesu, z tego powodu umieszczam na przyszłość tutorial'e:

* [Clound Path -> Microsoft Bot Framework Tutorial & Azure Bot Service Intro | Create a Chatbot](https://www.youtube.com/watch?v=530FEFogfBQ)

* [Cloud Path-> QnA Maker Bot | Microsoft Bot Framework tutorial](https://youtu.be/ZMhT9bRj_Ow)

#### Pricing (West Europe)

Opłaty związane związane z wykorzystywaniem serwisu QnAM Marker, zarówno API jak i portalu.

| Instancja | Koszt                                                        |
| --------- | ------------------------------------------------------------ |
| Bezpłatna | Kanał standardowy: nielimitowana ilość wiadomości<br />Kanał premium: 10k wiadomości miesięcznie |
| Standard  | $10 - Nielimitowana ilość zarządzanych dokumentów            |

Inne opłaty: 

* Azure App Service - wykorzystywanie mocy obliczeniowej, szczegółowy [cennik](https://azure.microsoft.com/en-us/pricing/details/app-service/windows/)

Przykładowe usług, które współpracują z Azure Bot Service:

* Cognitive Services pricing—Language Understanding (LUIS) [cennik](https://azure.microsoft.com/en-us/pricing/details/cognitive-services/language-understanding-intelligent-services/)
* Speech Service [cennik]()



