from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from azure_client import requestUrl, requestImg
import pandas as pd

app = Flask(__name__)
api = Api(app)


class BreedUrl(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('url', required=True);
        args = parser.parse_args()

        data, reason = requestUrl(args.url)
        return decode_data(data, reason)


class BreedImg(Resource):
    def post(self):
        img = request.get_data()
        data, reason = requestImg(img)
        return decode_data(data, reason)


def decode_data(data, reason):
    if reason != 'OK':
        return reason, 400

    df = pd.DataFrame(data)
    breed_rows = df.loc[df.probability > 0.6]
    if breed_rows.shape[0] == 0:
        respond = ({
            "isRecognized": False
        })
    else:
        breed_rows.sort_values(by=['probability'], ascending=False)
        breed_row = breed_rows.iloc[0]
        respond = ({
            "isRecognized": True,
            "breed": breed_row.tagName,
            "probability": breed_row.probability
        })
    return respond, 200


api.add_resource(BreedUrl, '/breedUrl')
api.add_resource(BreedImg, '/breedImg')

if __name__ == '__main__':
    app.run()  # run our Flask app
