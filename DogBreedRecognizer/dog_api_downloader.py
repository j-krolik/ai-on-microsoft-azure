
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
import os
from pathlib import Path
import requests
if sys.version_info >= (3, 0):
    # For Python 3
    from urllib import request as urlrequest, parse
else:
    # For Python 2
    import urllib as urlrequest
    import urlparse
import json


class DogAPI(object):

    def list(self):
        return self._api_request("breeds/list")

    def list_images(self, breed, subbreed=None):
        if subbreed is None:
            return self._api_request("breed/{}/images".format(breed))
        else:
            return self._api_request("breed/{}/{}/images".format(breed, subbreed))

    def random(self, breed=None, subbreed=None):
        if breed is None:
            return self._api_request("breeds/image/random".format(breed))
        if subbreed is None:
            return self._api_request("breed/{}/images/random".format(breed))
        else:
            return self._api_request("breed/{}/{}/images/random".format(breed, subbreed))

    def _api_request(self, endpoint):
        # print(self._build_url(endpoint)) # Useful for debugging, but not enough to implement a logger
        return json.loads(urlrequest.urlopen(
            parse.urljoin("https://dog.ceo/api/", endpoint)
        ).read().decode("utf-8"))


if __name__ == "__main__":

    dogapi = DogAPI()
    breeds = dogapi.list()['message']
    directory = os.path.join(os.getcwd(), 'breeds')
    for breed in breeds:
        breed_path = os.path.join(directory, breed)
        # if dir exist, don't download
        if os.path.isdir(breed_path):
            continue
        Path(breed_path).mkdir(parents=True, exist_ok=True)
        images_path = dogapi.list_images(breed)['message']

        sub_breed_name = None
        last_sub_breed_name = None
        for image_path in images_path:
            short_path = parse.urlparse(image_path)[2]
            _, _, breed_name_extended, file_name = short_path.split("/")

            extended_name_len = len(breed_name_extended.split('-'))
            if extended_name_len == 1:
                image_save_path = os.path.join(breed_path, file_name)
            elif extended_name_len == 2:
                _, sub_breed_name = breed_name_extended.split('-')
                if sub_breed_name != last_sub_breed_name:
                    last_sub_breed_name = sub_breed_name
                    sub_breed_dir = os.path.join(breed_path, sub_breed_name)
                    Path(sub_breed_dir).mkdir(parents=True, exist_ok=True)
                image_save_path = os.path.join(sub_breed_dir, file_name)
            else:
                continue
            image_result = requests.get(image_path)
            open(image_save_path, 'wb').write(image_result.content)
