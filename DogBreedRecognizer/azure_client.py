import http.client
import json
from azure_key import PREDICTION_KEY, PROJECT_ID, PUBLISHED_NAME

headers = {
    # Request headers
    'Content-Type': 'application/json',
    'Prediction-key': PREDICTION_KEY,
}


def requestUrl(url):
    body = '{"url": "' + url + '"}'
    try:
        conn = http.client.HTTPSConnection('westeurope.api.cognitive.microsoft.com')
        conn.request("POST", f"/customvision/v3.1/Prediction/{PROJECT_ID}/classify/iterations/{PUBLISHED_NAME}/url",
                     body, headers)
        response = conn.getresponse()
        data = json.loads(response.read())
        return data['predictions'], response.reason
    except Exception as e:
        # print("[Errno {0}] {1}".format(e.errno, e.strerror))
        None


def requestImg(body):
    try:
        conn = http.client.HTTPSConnection('westeurope.api.cognitive.microsoft.com')
        conn.request("POST", f"/customvision/v3.1/Prediction/{PROJECT_ID}/classify/iterations/{PUBLISHED_NAME}/image",
                     body, headers)
        response = conn.getresponse()
        data = json.loads(response.read())
        return data['predictions'], response.reason
    except Exception as e:
        # print("[Errno {0}] {1}".format(e.errno, e.strerror))
        None


if __name__ == '__main__':
    # test
    # print (requestUrl('https://images.dog.ceo/breeds/hound-afghan/n02088094_1003.jpg'))
    # print(requestUrl('https://images.d'))

    # path = "C:\\Users\\Jose\\OneDrive - Politechnika " \
    #        "Warszawska\\Studie-informatyka\\SEM2\\AZURE\\laby\\ai-on-microsoft-azure\\DogBreedRecognizer\\breeds" \
    #        "\\boxer\\28082007167-min.jpg "
    # with open(path, "rb")  as file:
    #     data = file.read()
    #     requestImg(data)

    None