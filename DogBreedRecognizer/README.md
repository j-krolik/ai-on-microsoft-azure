# DogBreedRecognizer

### Problematyka

Projekt ten jest testem, czy osoba o małej znajomości zagadnień widzenia maszynowego jest w stanie w prosty sposób uzyskać algorytm klasyfikujący zdjęcia wykorzystując Azure. W moim przypadku zadaniem jakim sobie postawiłem jest rozpoznawanie rasy psa.

### Use case

Projekt ten pomaga rozpoznać rasę psa użytkownika.

### Architektura

Aplikacja wykorzystuje:

* Azure Custom Vison
* REST API - Flask (Python)
* Baza ucząca [DogAPI](https://dog.ceo/dog-api/)

### Niezbędne kroki

1. Zagłębienie się w dokumentację.

   [Microsof Doc | Custom Vison Docs](https://docs.microsoft.com/en-us/azure/cognitive-services/Custom-Vision-Service/)

   [Microsof Doc | Quickstart: Build a classifier with the Custom Vision website](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/getting-started-build-a-classifier)

   [MS Cognitive Service| Classify Image API](https://southcentralus.dev.cognitive.microsoft.com/docs/services/Custom_Vision_Prediction_3.0/operations/5c82db60bf6a2b11a8247c15)

   Środowiskiem o najniższym poziomie wejścia, którym można bot jest `Microsoft Bot Framework Composer`. Jest to środowisko oparte o licencję MIT, które postanowiłem wykorzystać. Jest dostępne w wersji `desktopowej` oraz `web-based`

2. Przygotowanie danych uczących. Uruchomienie program `dog_api_downloader.py`  powoduje utworzenie katalogu *breed* oraz umieszczenie w nim zdjęć różnych ras psów, każda klasa jest w osobnym katalogu.

3. Uczenie sieci oraz jest testowanie z wykorzystaniem [Custom Vison Portal](https://www.customvision.ai/projects) oraz konta Azure. Niezbędne kroki są przedstawione [tutaj](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/getting-started-build-a-classifier)

4. Publikacja nauczonej sieci, ucieszenie danych o adresie końcówki oraz klucza w pliku, z którego informacje te pobiera utworzony `server.py`

### Działanie - film

[YT film](https://youtu.be/pSn1OtDW0i4)