# Bot

### Problematyka

Początkowo bot miał być dla Stacji Informacyjnych, aby można było zapytać się o najnowsze informacje. Jednakże z tego względu, że większa część użytkowników nie może się "przekonać" do chat bota postanowiłem dodać podobną  podobną funkcjonalność - zamiast informacji podawać żarty.

### Niezbędne kroki

1. Zagłębienie się w dokumentację oraz obejrzenie poradników

   [YT | Arafat Tehsin](https://youtu.be/2Psu6qHPAL0)

   [YT | Cloud Path](https://www.youtube.com/watch?v=530FEFogfBQ)

   [Microsof Doc | The Bot Framework Composer](https://docs.microsoft.com/en-us/composer/tutorial/tutorial-introduction)

   [Microsfot Doc | Bot Framework SDK](https://docs.microsoft.com/en-us/azure/bot-service/bot-builder-tutorial-create-basic-bot?view=azure-bot-service-4.0&tabs=csharp%2Cvs)

   [GitHub | Microsoft Bot Framework Composer](https://github.com/microsoft/botframework-composer)

   [Github | BotBuilder-Samples ](https://github.com/Microsoft/BotBuilder-Samples)

   Środowiskiem o najniższym poziomie wejścia, którym można bot jest `Microsoft Bot Framework Composer`. Jest to środowisko oparte o licencję MIT, które postanowiłem wykorzystać. Jest dostępne w wersji `desktopowej` oraz `web-based`

2. Instalowanie środowiska. Wymagane narzędzia:

   * [Bot Framework Emulator](https://github.com/microsoft/BotFramework-Emulator/releases/latest)
   *  [.NET Core SDK 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
   * Bot Framework Composer: [Windows](https://aka.ms/bf-composer-download-win) | [macOS](https://aka.ms/bf-composer-download-mac) | [Linux](https://aka.ms/bf-composer-download-linux).

### Architektura

Aplikacja wykorzystuje:

* Azure Bot Service
* Azure Cognitive Services (LUIS)
* REST API z kawałmi [JokeAPI](https://sv443.net/jokeapi/v2/)

### Działanie - film

[YT film](https://youtu.be/bD8Za7B_Izw)



