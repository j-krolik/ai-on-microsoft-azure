# Process and classify images with the Azure cognitive vision services

## Computer vision API

### Intro

Usługa jest częścią Azure Cognitive Services. Jest on odpowiedzią Azure na potrzeby standardowego przetwarzania obrazów z wykorzystaniem sztucznej inteligencji. 

Wybrane możliwości

* Face API

  Możliwości:

  * weryfikacja - określia prawdopodobieństwo należenia dwóch twarzy do jednej osoby 
  * detekcja - wykrywa ludzkie twarze na zdjęciach
  * identyfikację - wyszukuje oraz identyfikuje twarze
  * podobieństwo - wyszukuje podobne twarze
  * grupowanie - grupuje niezidentyfikowane twarze , bazując na podobieństwach

  Typowe informacje uzyskiwane ze zdjęcia:

  * lokalizacjia - składowe prostokąta obejmującego twarz (góra, lewy, wysokość, szerokość)
  * punkty charakterystyczne (landmarks) - położenie punktów charakterystycznych twarzy takich jak nos, oczy brwi itd. (do 27 punktów)
  * atrybuty osoby:
    * Age
    * Gender
    * Smile intensity
    * Facial hair
    * Head pose (3D)
    * Emotion

* Emotion API

  Analizuje twarze i określa poziom pewności dla cech (emocji). W JSON'ie otrzymujemy położenie kwadratu twrazy (góra, lewy, wysokość, szerokość) oraz emocjie takie jak:

  * złość 
  * pogarda 
  * zniesmaczenie 
  * strach
  * szczęście
  * neutralność (brak emocji)
  * smutek
  * zaskoczenie

* OCR ( ang. optical character recognition)
  Optyczne rozpoznawanie znaków zarówno drukowanych jak i pisanych.
* Opisywanie obrazów (zawartości), detekcja obiektów, generowanie miniaturek (thumbnail), tagowanie zdjęc itd.

###  Use cases

* Identyfikacja oraz uwierzytelnienie osób na podstawie twarzy
* Badanie zadowolenia konsumentów na podstawie emocji wyrażanych na twarzy
* System sprawdzający czy obliczenia zapisane na kartce są poprawne
* Program segregujący zdjęcia w telefonie (ze względu na osoby okoliczności itp)
* Detekcja osób w miejscach gdzie nie powinien nikt wchodzić

### How to

**Jak użyć?**

Potrzebujemy zasób `face`, który znajduje się w `AI + Machine Learning`. Jeśli nie posiadamy to na [portaulu Azure](https://portal.azure.com/) tworzymy zasób Azure Cognitive Services, który generuje nam klucze oraz końcówki (endpoint) umożliwające korzystanie z tej usługi. 

Dokumetacja API jest [tutaj](https://westeurope.dev.cognitive.microsoft.com/docs/services/563879b61984550e40cbbe8d/operations/563879b61984550f30395236) 

Przykład wykorzystywania dla:

* Face API [przykład](https://docs.microsoft.com/en-us/learn/modules/identify-faces-with-computer-vision/8-test-face-detection?pivots=python)
* Emotion APi [przykład](https://docs.microsoft.com/en-us/learn/modules/identify-faces-with-computer-vision/10-using-emotion-api)

Wymagania dotyczące obrazów:

- Obsługiwane formaty: JPEG, PNG, GIF, BMP.
- Rozmiar pliku mniejszy niż 4 MB.
- Rozmiar obrazu conajmniej 50 x 50.

**Pricing (West Europe)**

* Face API, Emotion API [pricing][https://azure.microsoft.com/en-us/pricing/details/cognitive-services/face-api/]

| Instancja                | Liczba transakcji na sekundę (TPS) | Możliwości                                                   | Koszt                                                        |
| ------------------------ | ---------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Bezpłatna - web/kontener | 20 tranzakcji na minutę            | Rozpoznawanie twarzy<br />Weryfikacja twarzy<br />Identyfikacja twarzy<br />Grupowanie twarzy<br />Szukanie podobnych twarzy | 30k tranzakcji miesięcznie za darmo                          |
| Standard - web/kontener  | 10 TPS                             | Rozpoznawanie twarzy<br />Weryfikacja twarzy<br />Identyfikacja twarzy<br />Grupowanie twarzy<br />Szukanie podobnych twarzy | 0-1M tranazkcji - $1 za każde 1k tranzakcji<br/>1M-5M tranazkcji - $0.80 za każde 1k tranazkcji<br/>5M-100M tranazkcji - $0.60 za każde 1k tranazkcji<br/>100M+ tranazkcji - $0.40 za każde 1k tranazkcji |
|                          |                                    | Przechowywanie twarzy                                        | $0.01 za każde 1k twarzy miesięcznie                         |

* Computer Vison [pricing](https://azure.microsoft.com/en-us/pricing/details/cognitive-services/computer-vision/)

| Instancja                | Liczba transakcji na sekundę (TPS) | Możliwości                                                   | Koszt                                                        |
| ------------------------ | ---------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Bezpłatna - web/kontener | 20 tranzakcji na minutę            |                                                              | 5k tranzakcji miesięcznie za darmo                           |
| Standard - web/kontener  | 10 TPS                             | Tag<br />Face<br />GetThumbnail<br />Color<br />Image Type<br />GetAreaOfInterest | 0-1M tranazkcji - $1 za każde 1k tranzakcji<br/>1M-5M tranazkcji - $0.80 za każde 1k tranazkcji<br/>5M-100M+ tranazkcji - $0.65 za każde 1k tranazkcji |
|                          |                                    | OCR<br/>Adult<br/>Celebrity<br/>Landmark<br/>Detect, Objects<br/>Brand | 0-1M tranazkcji - $1.50 za każde 1k tranzakcji<br/>1M-5M tranazkcji - $1 za każde 1k tranazkcji<br/>5M-100M+ tranazkcji - $0.65 za każde 1k tranazkcji |
|                          |                                    | Describe<br />Read                                           | $2.50 za każde 1k tranzakcji                                 |



## Custom Vision

### Intro

Najnowsza usługa oferowana przez Azure Cognitive Services. Pozwala ona etykietowanie obrazów na podstawie nauczonej przez nas wcześniej sieci. Do uczenia sieci należy wykorzustać uprzednio przygotowany zbiór uczący zawierający etykietowane zdjęcia.

###  Use cases

* Aplikacja dla grzybiarzy rozpoznająca najczęściej występujące grzyby w lesie
* System segregacji pomieszanych przedmiotów produkowanych na lini produkcyjnej.

### How to

**Jak użyć?**

W zależności od wymagań do uczenia możemy wykorzystać:

* dedykowany portal  [Custom Vision Service portal](https://www.customvision.ai/)
* API - [dokumentacja uczenie](https://westeurope.dev.cognitive.microsoft.com/docs/services/Custom_Vision_Training_3.0/operations/5c771cdcbf6a2b18a0c3b802)

W przypadku predykcji wykorzystuje się API - [dokumentacja predykcja](https://westeurope.dev.cognitive.microsoft.com/docs/services/Custom_Vision_Prediction_3.0/operations/5c82db60bf6a2b11a8247c15)

**Pricing (West Europe)**

[pricing][https://azure.microsoft.com/en-us/pricing/details/cognitive-services/custom-vision-service/]

| Instancja | Liczba transakcji na sekundę (TPS) | Możliwości                                                   | Koszt                                                        |
| --------- | ---------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Bezpłatna | 2 TPS                              | Tranzakcja wgrania, trenowania oraz predykcji.<br />Do 2 projektów<br />Do 1 godziny treningu miesięcznie | 5k obrazów treningowych na projekt za darmo<br />10k predykcji miesięcznie za darmo |
| Standard  | 10 TPS                             | Tranzakcja wgrania oraz predykcji.<br />Do 100 projektów     | $2 za każde 1k tranzakcji                                    |
|           |                                    | Trenowanie                                                   | $20 za każdą 1h trenowania                                   |
|           |                                    | Przechowywanie zdjęć<br />Do 6MB każde                       | $0.70 za każde 1k zdjęć                                      |
## Video Indexer

### Intro

Usługa oferowana przez Azure Cognitive Services. Umożliwia ona analizę nagrań wide. 

Przykładowe cechy które można wydobyć:

* transkrypt - linie tekstu wraz z czasem rozpoczęcia oraz zakończenia
* OCR - optyczne rozpoznawanie znaków
* słowa kluczowe - na podstawie mowy lub rozpoznanego tekstu
* etykietowanie
* rozpoznawanie marek - na podstawie mowy lub rozpoznanego tekstu
* rozpoznawanie twarzy
* opinia/zdanie - pozytywna, neutralna, negatywna
* emocje

###  Use cases

* Dodawanie napisów do filmów dla niesłyszących.
* Program porządkujący nagrania na komputerze

### How to

**Jak użyć?**

Do uzyskania klucza API można wykorzystać dedykowany portal portal [Video Indexer Portal](https://www.videoindexer.ai/account/login)

SDK można wykorzystywać za pomocą Pythona lub C#. Zalecane środowisko to VS Code, w przypadku Pythona jest dodatkowe [rozszeczenie](https://marketplace.visualstudio.com/items?itemName=ms-python.python) dla VSC

Źródłem nagrań może być link URL (zalezany), plik wysłany jako tablica bajtów lub referencja to istniejącego nagrania za pomocą ID.

Obsługiwane formaty nagrań: WMV, MOV, MPG, AVI.

[Dokumentacja API](https://api-portal.videoindexer.ai/), [Tutorial](https://docs.microsoft.com/en-us/azure/media-services/video-indexer/video-indexer-use-apis)

**Pricing (West Europe)**

[pricing][https://azure.microsoft.com/en-us/pricing/details/cognitive-services/video-indexer/]

| Instancja | Możliwości                                               | Koszt        |
| --------- | -------------------------------------------------------- | ------------ |
| Bezpłatna | Indeksowanie za pośrednictwem pośrednictwem przeglądarki | 10h za darmo |
|           | Indeksowanie za pośrednictwem API                        | 40h za darmo |

W przypadku większych potrzeb należy wykorzystać jednostkę [S3 media reserved units.](https://docs.microsoft.com/en-us/azure/media-services/previous/media-services-scale-media-processing-overview) Dodatkowe opłąty mogą być również:

- [Audio/Video Analysis](https://azure.microsoft.com/en-us/pricing/details/media-services/#analytics) 
- [Encoding](https://azure.microsoft.com/en-us/pricing/details/media-services/#encoding) 
- [Streaming](https://azure.microsoft.com/en-us/pricing/details/media-services/#streaming) 
- [Storage](https://azure.microsoft.com/en-us/pricing/details/storage/)
- [Network egress](https://azure.microsoft.com/en-us/pricing/details/bandwidth/) 
- [Media Reserved Units](https://azure.microsoft.com/en-us/pricing/details/media-services/#encoding)