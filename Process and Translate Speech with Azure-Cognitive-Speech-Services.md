# Process and Translate Speech with Azure Cognitive Speech Services

## Transcribe speech input to text

### Intro

Usługa jest częścią Azure Cognitive Services. 

Analizuje nagrania głosowe i zamienai je na tekst, działa w czasie rzeczywistym. 

Wykorzystuje ona uczenie maszynowe. 

Do dyspozycji mamy zarówno client SDK (C# & Python) jak i (REST) API.

[Wspierane języki](https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/language-support)

###  Use cases

* Dodawanie napisów do filmów,
* Sterowanie głosowe
* Tłumaczenie mowy w czasie rzeczywsitym

### How to

**Jak użyć?**

Potrzebujemy `Azure Cognitive Services`. Jeśli nie posiadamy to na [portaulu Azure](https://portal.azure.com/) tworzymy zasób Azure Cognitive Services, który generuje nam klucze oraz końcówki (endpoint) umożliwające korzystanie z tej usługi.

W przypadku SDK, dobrze wykorzystywać dedykowane VSC. 

**Pricing (West Europe)**

| Instancja                                         | Opcja         | Koszt                                                        |
| ------------------------------------------------- | ------------- | ------------------------------------------------------------ |
| Bezpłatna<br />Maksymalnie 1 równoczesne rządanie | Standardowa   | 5h audio miesięcznie za darmo                                |
|                                                   | Dostosowana   | 5h audio miesięcznie za darmo<br />1 model końcówki (endpoint'a) miesięcznie za darmo |
|                                                   | Wielokanałowa | 5h audio miesięcznie za darmo                                |
| Standard<br />Maksymalnie 20 równoczesnych rządań | Standardowa   | $1 za każdą godzinę audio                                    |
|                                                   | Dostosowana   | $1.40 za każdą godzinę audio<br />$1.2904 za każdą godzinę końcówki (endpoint'a) |
|                                                   | Wielokanałowa | $2.10 za każdą godzinę audio                                 |

## Synthesize Text Input to Speech

### Intro

Usługa jest częścią Azure Cognitive Services. 

Syntezuje text na mowę ludzką w czasie rzeczywistym. 

Wykorzystuje ona uczenie maszynowe. 

Do dyspozycji mamy zarówno client SDK (C# & Python) jak i (REST) API.

[Wspierane języki](https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/language-support)

###  Use cases

* Wsparce osób niewidomych
* Automat sprzedający, kasy samo-obsługowe
* Aplikacja ucząca prawidłowej mowy.

### How to

**Jak użyć?**

Potrzebujemy `Azure Cognitive Services`. Jeśli nie posiadamy to na [portaulu Azure](https://portal.azure.com/) tworzymy zasób Azure Cognitive Services, który generuje nam klucze oraz końcówki (endpoint) umożliwające korzystanie z tej usługi.

W przypadku SDK, dobrze wykorzystywać dedykowane VSC. 

**Pricing (West Europe)**

| Instancja                                         | Opcja                                     | Koszt                                                        |
| ------------------------------------------------- | ----------------------------------------- | ------------------------------------------------------------ |
| Bezpłatna<br />Maksymalnie 1 równoczesne rządanie | StandardowaOparta o sieci neuronowe       | 5M znaków miesięcznie za darmo                               |
|                                                   | Dostosowana                               | 5M znaków miesięcznie za darmo<br />1 model końcówki (endpoint'a) miesięcznie za darmo |
|                                                   | Oparta o sieci neuronowe                  | 0.5M znaków miesięcznie za darmo                             |
| Standard<br />Maksymalnie 20 równoczesnych rządań | Standardowa                               | $4 za każdy 1M znaków                                        |
|                                                   | Dostosowana                               | $6 za każdy 1M znaków<br />$0.0537 za każdą godzinę końcówki (endpoint'a) |
|                                                   | Oparta o sieci neuronowe                  | $16 za każdy 1M znaków<br />W przypadku długich nagrań audio $100 za każdy 1M znaków |
|                                                   | Dostosowana oraz oprata o sieci neuronowe | Generowanie mowy: [kontakt](https://aka.ms/custom-neural-gating-overview)<br />W czasie rzeczywistym $24 za każdy 1M znaków<br />$4.04 za każdą godzinę końcówki (endpoint'a)<br />W przypadku długich nagrań audio $100 za każdy 1M znaków |
## Transcribe speech input to text

### Intro

Usługa jest częścią Azure Cognitive Services. 

Analizuje nagrania głosowe i zamienai je na tekst, działa w czasie rzeczywistym. 

Wykorzystuje ona uczenie maszynowe. 

Do dyspozycji mamy zarówno client SDK (C# & Python) jak i (REST) API.

[Wspierane języki](https://docs.microsoft.com/en-us/azure/cognitive-services/speech-service/language-support)

###  Use cases

* Spotkania biznesowe
* Na wakacjach
* Tłumaczenie filmów

### How to

**Jak użyć?**

Potrzebujemy `Azure Cognitive Services`. Jeśli nie posiadamy to na [portaulu Azure](https://portal.azure.com/) tworzymy zasób Azure Cognitive Services, który generuje nam klucze oraz końcówki (endpoint) umożliwające korzystanie z tej usługi.

W przypadku SDK, dobrze wykorzystywać dedykowane VSC. 

**Pricing (West Europe)**

| Instancja                                         | Opcja       | Koszt                         |
| ------------------------------------------------- | ----------- | ----------------------------- |
| Bezpłatna<br />Maksymalnie 1 równoczesne rządanie | Standardowa | 5h audio miesięcznie za darmo |
| Standard<br />Maksymalnie 20 równoczesnych rządań | Standardowa | $2.50 za każdą godzinę audio  |