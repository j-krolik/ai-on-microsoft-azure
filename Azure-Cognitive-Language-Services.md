# Evaluate text with Azure Cognitive Language Services

## Conctent Moderator

### Intro

Moderowanie treści takich jak zdjęcia, tekst czy wideo, nierzadko są czasochłonne. Z tego Azure dostarcza serwis `Azure Conctent Moderator`. Pozwala on na maszynowe sprawdzenie czy dana treść może wymagać sprawdzenia moderatora.

W tym dokumencie skupimy się tylko i wyłącznie części serwisu obsługującej tekst.

Serwis ten używamy za pomocą Text Moderation API, którego funkcjonalność stanowi:

* **Profanity**
  Zwrócenie wszystkich znalezionych w przesłanych tekście potencjalnie niepożądanych wyrazów. Informacjami jakie uzyskujemy jest: wyraz oraz lokalizacja w tekście. Jest możliwość tworzenia własnych list wyrazów.

* **Clasification**

  Ta funkcjonalność sprawdza czy przesłany tekst zawiera treści obraźliwe lub na tle seksualnym. W odpowiedzi uzyskujemy informacje czy zalecane jest sprawdzenie przesłanej treści oraz wyniki przynależności od 0 do 1 do trzech kategorii: 

  * Kategoria 1: Potencjalne występowanie sformułowań, które w pewnych sytuacjach mogą być odebrane jednoznacznie za treści dla dorosłych lub o charakterze seksualnym.
  * Kategoria 2: Potencjalne występowanie sformułowań, które w pewnych sytuacjach mogą być odebrane jako treści z podtekstem seksualnym .
  * Kategoria 3: Potencjalne występowanie sformułowań, które mogą być odebrane jako obraźliwe.

* **Personally identifiable information**

  Funkcjonalność, sprawdza czy w tekście znajdują się dane osobowe. Między innymi: adres mailowym, IP adres, adres (USA), numer telefonu (USA,UK).
  
###  Use cases

* Filtr w chatboot'cie blokujące agresywne osoby.
* Blokowanie treści o charakterze seksualnym w ogólnodostępnym czacie internetowym.
* Poszukiwanie danych wrażliwych (dane osobowe) na stronkach internetowych.
* Poszukiwanie niepożądanych treści na forum internetowym. 

### How to

**Jak użyć?**

1. Logujemy się na [portaulu Azure](https://portal.azure.com/)
2. Na panelu po lewej stronie klikamy *Create a resource*
3. Wyszukujemy i wybieramy *Content Moderator*
4. Wybieramy subskrypcję
5. Podajemy unikatową nazwę zasobu oraz wybieramy lokalizację.
6. Wybieramy szczebel cenowy: bezpłatny F0 lub płatny S0
7. Klikamy *Create*
8. Po wejściu w utworzony zasób, w zakładce `keys and endpoint` odnajdujemy klucze oraz adres utworzonego API.  Jeden z tych kluczy należy zawrzeć w zapytaniu.
9. Dokumentację API można znaleźć [tutaj](https://docs.microsoft.com/en-us/azure/cognitive-services/Content-Moderator/api-reference)

**Pricing (West Europe)**

Każde zapytanie tekstowe powinno posiadać maksymalnie 1024 znaki.

| Instancja | Liczba transakcji na sekundę (TPS) | Koszt                                                        |
| --------- | ---------------------------------- | ------------------------------------------------------------ |
| Bezpłatna | 1TPS                               | 5k transakcji miesięcznie za darmo                           |
| Standard  | 10TPS                              | 0-1M transakcji- $1 za każde1,000 transakcji<br />1M-5M transakcji- $0.75 za każde1,000 transakcji<br/>5M-10M transakcji- $0.60 za każde1,000 transakcji<br/>10M+ transakcji- $0.40 za każde 1,000 transakcji |

## Language Understanding Intelligent Service (LUIS) 

### Intro

LUIS jest częścią ` Azure Cognitive Services API `. Na podstawie uczenia maszynowego pozwala ona na rozumienie dostarczonej treści. W odpowiedzi na przesłaną treści przesyła informacje o ogólnej tematyce wypowiedzi oraz ewentualnie poszczególne informacje.

LUSI analizuje tekst pod względem trzech aspektów:

* **Wypowiedzi**
  Wypowiedź użytkownika, którą aplikacja musi zinterpretować.
* **Intencji**
  Intencja reprezentuje zdanie lub działanie, które użytkownik chce wykonać. Jest to cel lub zadanie wyrażone w wypowiedzi użytkownika
* **Encji** 
  Encja reprezentowana jest jako słowo, sformułowanie lub prostych danych jak np. nazwa miasta, liczba.

### Use cases
W aplikacjach gdzie chcemy analizować ludzkie polecenia np.

* system zamawiania pizzy

* system informowania o aktualnym stanie przesyłki, zamówienia  w postaci chatbot'a

### How to

**Jak użyć?**

1. Tworzymy zasób 
   * Logujemy się na [portaulu Azure](https://portal.azure.com/)
   * Na panelu po lewej stronie klikamy *Create a resource*
   * Wyszukujemy i wybieramy *LUIS*
   * Wybieramy subskrypcję
   * Podajemy unikatową nazwę zasobu oraz wybieramy lokalizację.
   * Wybieramy szczebel cenowy: bezpłatny F0, F1 lub płatny S0
   * Klikamy *Create*
   * Po wejściu w utworzony zasób, w zakładce *keys* odnajdujemy klucze
2. Tworzymy aplikację
   * Wchodzimy na stronę https://eu.luis.ai/
   * Logujemy się tymi samymi danymi, co w przypadku portalu Azure oraz wybieramy uprzednio utworzony zasób
   * Tworzymy aplikację, podajemy nazwę oraz kulturę
   * Tworzymy intencje wewnątrz, których podajemy przykładowe zwroty (min 5)
   * Tworzymy istotne encje, oraz łączymy je z intencjami.
   * Trenujemy model
   * Tworzymy endpoint'a, do zapytań wykorzystujemy jeden z uprzednio wygenerowanych kluczy 

**Pricing  (West Europe)**

Serwis może być dostarczany z web endpoint'a lub kontenera.

| Instancja | Liczba transakcji na sekundę (TPS)* | Opcja                  | Koszt                                  |
| --------- | ----------------------------------- | ---------------------- | -------------------------------------- |
| Bezpłatna | 5TPS                                | Analiza tekstu         | do 10k transakcji miesięcznie za darmo |
|           |                                     | Autoryzacja transakcji | do 1M transakcji miesięcznie za darmo  |
| Standard  | 50TPS                               | Analiza tekstu         | $1.50 za każde 1,000 transakcji        |
|           |                                     | Analiza mowy           | $5.50 za każde 1,000 transakcji        |

*Ograniczenie liczba transakcji na sekundę dotyczy tylko dotyczy web endpoint'a.

## Text Analytics

### Intro
Text Analytics API jest częścią  `Cognitive Service`. Zostało zaprojektowane aby wydobywać informacje z tekstu. Serwis ten potrafi:

* zidentyfikować język
* zbadać nastrój autora
* wydobywać kluczowe wyrażenia
* wykrywać znane encje

### Use cases
Kiedy przydatną informacją jest nastrój osoby analizowanej.

* Algorytm dobierający reklamy na stronach internetowych, jeden z czynników - nastrój 
* Analiza zadowolenia z danego produktu/usługi na podstawie opinii zamieszczonych na stronie internetowej.

### How to

**Jak użyć?**

1. Logujemy się na [portaulu Azure](https://portal.azure.com/)
2. Na panelu po lewej stronie klikamy *Create a resource*
3. Wyszukujemy i wybieramy *Text Analytics*
4. Wybieramy subskrypcję
5. Podajemy unikatową nazwę zasobu oraz wybieramy lokalizację.
6. Wybieramy szczebel cenowy
7. Klikamy *Create*
8. Po wejściu w utworzony zasób, w zakładce `keys and endpoint` odnajdujemy klucze oraz adres utworzonego API.  Jeden z tych kluczy należy zawrzeć w zapytaniu.
9. Dokumentację API można znaleźć [tutaj](https://docs.microsoft.com/en-us/azure/cognitive-services/text-analytics/quickstarts/python)

**Pricing (West Europe)**

Każde zapytanie tekstowe powinno posiadać maksymalnie 1000 znaki.

Serwis może być dostarczany z web endpoint'a lub kontenera.

| Instancja | Opcja                                                        | Koszt                                                        |
| --------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Bezpłatna | Analiza semantyczna<br />Ekstrakcja kluczowych sformułować<br />Detekcja języka<br /><br />Wykrywanie znanych encji* | 5k transakcji miesięcznie za darmo                           |
| Standard  | Analiza semantyczna<br />Ekstrakcja kluczowych sformułować<br />Detekcja języka<br /><br />Wykrywanie znanych encji* | 2.50-0.5M transakcji- $2 za każde 1,000 transakcji<br />0.5M-2.5M transakcji- $1 za każde1,000 transakcji<br/>5M-10M transakcji- $0.50 za każde1,000 transakcji<br/>10M+ transakcji- $0.25 za każde 1,000 transakcji |

*Nie dostępne dla kontenera.

Dostępne są również instancje S0, S1, S2, S3, S4.
